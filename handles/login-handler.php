<?php 
if(isset($_POST['login'])){
    $username = $_POST['username'];
    $password = $_POST['password'];
    $result = $event->loginUser($username,$password);
    if($result){
        session_start();
        $_SESSION["loggedin"] = true;
        $_SESSION["id"] = $result['id'];
        $_SESSION["username"] = $result['name'];    
        $_SESSION["role"] = $result['role'];
        $_SESSION['email'] = $result['email'];
        header("Location: index.php");
    }else{
        header("Location: infopage.php?msg=" . 'Error when login user');
    }
}
?>