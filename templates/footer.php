<footer class="page-footer font-small">
  <div class="text-center text-md-left footer-info">
    <div class="row">
      <div class="col-md-6 mt-md-0 mt-3">
        <h5 class="text-uppercase">Why we?</h5>
        <p>Maybe our pizza is not the best but we make her with heart</p>
        <p>And deliver 24/7</p>
      </div>
      <hr class="clearfix w-100 d-md-none pb-3">
      <div class="col-md-3 mb-md-0 mb-3 contact-info">
        <h5 class="text-uppercase">Contact</h5>
        <address>
            <p><i class="fa fa-map-marker"></i> 300 Kershaw Canyon Road, Caliente, NV 89008, Stany Zjednoczone</p>
            <p><i class="fa fa-phone"></i> phone: 723-949-123</p>
            <p><i class="fa fa-envelope"></i> email: <a href="mailto:pizza@24.com">pizza@24.com</a></p>
        </address>
      </div>
      <hr class="clearfix w-100 d-md-none pb-3">
      <div class="col-md-3 mb-md-0 mb-3 social-media">
        <h5 class="text-uppercase">Our chanels</h5>
        <ul class="list-unstyled ">
          <li>
            <a href="#!" aria-label="Facebook" target="_blank" rel="noopener" class="fa fa-facebook"></a>
          </li>
          <li>
            <a href="#!" aria-label="Youtube" target="_blank" rel="noopener" class="fa fa-youtube"></a>
          </li>
          <li>
            <a href="#!" aria-label="Instagram" target="_blank" rel="noopener" class="fa fa-instagram"></a>
          </li>
        </ul>
      </div>
    </div>
  </div>
    <div class="footer-copyright text-center">
      © <?php echo date("Y"); ?> Copyright: RG
    </div>
</footer>
<script src="https://js.stripe.com/v3/"></script>

<script src="./assets/js/stripe.js"></script>
<script type="text/javascript" src="./totalPriceCalc.js"></script>
</body>