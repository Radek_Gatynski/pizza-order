<?php
session_start();
include './templates/header.php';
include './classes/DBConfig.php';
$conn = new DBConfig();
include './classes/Pizza.php';
$newPizza = new Pizza($conn);
$pizzas = $newPizza->getMenu();
$sizes = $newPizza->getPizzaSizes();
include './handles/order-pizza-handler.php';
$userID = $_SESSION['id'];
?>
<div class="container-fluid">
    <div class="row">
        <div class="container pizza-details text-center">
            <form action="add.php" method="POST" style="width:968px">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">Pizza Name</th>
                            <th scope="col">Ingredients</th>
                            <th scope="col">Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($pizzas as $pizza) : ?>
                        <tr>
                            <?php if ($pizza['id'] == 1) : ?>
                              <td><label><input class="input-field" checked="checked" type="radio" name="formPizzaSelected[]" value="<?php echo ($pizza['id']); ?>" id="<?php echo ($pizza['id']); ?>" /><span></span></label></td>
                                <?php else : ?>
                               <td> <label><input class="input-field" type="radio" name="formPizzaSelected[]" value="<?php echo ($pizza['id']); ?>" id="<?php echo ($pizza['id']); ?>" /><span></span></label></td>
                            <?php endif; ?>
                            <td><?php echo ($pizza['title']); ?></td>
                            <td><?php echo ($pizza['ingredients']); ?></td>
                            <td class='price'><?php echo ($pizza['price']) . 'zł'; ?></td>

                        </tr>

                        <?php endforeach; ?>
                        
                        <tr id="selectedSize">
                            <?php foreach ($sizes as $size) : ?>
                            <td>
                                <?php if ($size['id'] == 1) : ?>
                                <label><input name="pizzaSizeSelected[]" checked="checked" value="<?php echo ($size['id']); ?>" type="radio" />
                                    <span><?php echo $size['name'] . " + " . $size['price_plus'] . "zl"; ?> </span>
                                    <span><?php echo $size['name'] . " + " . $size['price_plus'] . "zl"; ?> </span>
                                    <?php else : ?>
                                    <label><input name="pizzaSizeSelected[]" value="<?php echo ($size['id']); ?>" type="radio" />
                                        <span><?php echo $size['name'] . " + " . $size['price_plus'] . "zl"; ?> </span>
                                        <?php endif; ?>
                            </td>
                            <?php endforeach; ?>
                        </tr>
                        <td><span>Price: </span></td>
                        <td><span id="totalPriceForPizza">Price: </span></td>
                    </tbody>
                </table>
                <input type="text" class="deliveryPlace" name="orderAddres" placeholder="Place to deliver (address)" required />
                <div class="center">
                    <input type="submit" name="submit" value="Order" class="btn btn-lg btn-primary">
                </div>
            </form>
        </div>
    </div>
</div>
<?php include('templates/footer.php'); ?>

</html>