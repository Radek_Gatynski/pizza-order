<?php
session_start()
?>
<!DOCtYPE html>
<html lang="en">
<?php include('./templates/header.php'); ?>
<div class="container-fluid main-page">
    <section class="container">
        <header>
            <h1 class="text-center">The pizza you like</h1> 
        </header>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4">
                <h3 class="text-center">Our menu</h3>
                <a href="menu.php" class="btn btn-info">Read more</a>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4">
                <h3 class="text-center">Order and delivery</h3>
                <a href="#" class="btn btn-info">Read more</a>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4">
                <h3 class="text-center">current promotions</h3>
                <a href="#" class="btn btn-info">Read more</a>
            </div>
        </div>
    </section>           
</div>
<?php include('./templates/footer.php'); ?>
</html>