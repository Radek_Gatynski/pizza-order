$(document).ready(function(){
    $("#totalPriceForPizza").text("Price: 16zł");
    var sizePriceTable = [0,2,5,10];
    var pizzaPrice = [16,17,19,20,21,24,21,24];
    $("#selectedSize input[type='radio']").click(function(){
        var radioValue = $("input[name='pizzaSizeSelected[]']:checked").val();
        if(radioValue){
            var pizzaValue = $("input[name='formPizzaSelected[]']:checked").val();            
            $("#totalPriceForPizza").text("Price: " + (sizePriceTable[radioValue-1] + pizzaPrice[pizzaValue-1] ));
        }
    });
    $("#pizzaListTable input[type='radio']").click(function(){
        var radioValue = $("input[name='formPizzaSelected[]']:checked").val();
        if(radioValue){
            var sizeValue = $("input[name='pizzaSizeSelected[]']:checked").val();            
            $("#totalPriceForPizza").text("Price: " + (pizzaPrice[radioValue-1] + sizePriceTable[sizeValue-1] ));
        }
    });
});