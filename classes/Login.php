<?php 
class Login{
    private $errors;
    private $pdo;
    public function __construct($conn){
        $this->errors = array();
        $this->pdo = $conn->connect();
    }

    public function loginUser($username,$password){
        return $this->getUserInfo($username,$password);
    }

    private function getUserInfo($username, $password){
        $sql = "SELECT * FROM users WHERE name = ?";
        if($stmt = $this->pdo->prepare($sql)){
            $stmt->bindParam(1,$username);
            if($stmt->execute()){
                $user = $stmt->fetch();
                if($user && password_verify($password,$user['password'])){
                    return $user;
                }else{
                    return false;
                }
            }
        }
    }
    public function getError($msg_error){
        if(!in_array($msg_error, $this->errors)){
            $msg_error = "";
        }
        return "<small class='form-text text-muted'>$msg_error</small>";
    }
}
?>