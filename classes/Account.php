<?php 
class Account{
    private $pdo;
    private $errors;
    private $id;
    public function __construct($conn,$id){
        $this->id=$id;
        $this->pdo = $conn->connect();
    }
    private function getUsername(){
        $sql = "SELECT name FROM users where id=?";
        if($stmt = $this->pdo->prepare($sql)){
            $stmt->bindParam(1,$this->id);
            if($stmt->execute()){
                if($stmt->rowCount()==1){
                    $stmt = $stmt->fetch;
                    return $stmt['name'];
                }else{
                    return false;
                }
            }
        }
    }
}
?>