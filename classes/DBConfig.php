<?php
class DBConfig{
    private $hostname = 'localhost';
    private $database = 'pizzaorder';
    private $user = 'testingUser';
    private $password = 'ht83%4JJFE1';
    private $pdo;
    public function connect(){
        try{
            $this->pdo = new PDO("mysql:host=" . $this->hostname . ";dbname=" . $this->database,$this->user,$this->password);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $this->pdo;
        }catch(PDOException $e){
            die("Connection error: " . $e->getMessage());
        }
    }
}
?>