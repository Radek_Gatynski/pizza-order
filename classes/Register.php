<?php 
class Register{
    private $errors;
    private $pdo; 
    public function __construct($conn){
        $this->errors = array();
        $this->pdo = $conn->connect();
    }
    public function addNewUser($username,$email,$password,$repeatPassword){
        $this->validateUsername($username);
        $this->validateEmail($email);
        $this->validatePassword($password,$repeatPassword);
        if(empty($this->errors)){
            $result = $this->insertToDatabase($username,$email,$password);
            return $result;
        }else{
            return false;
        }
    }
    private function validateUsername($username){
        if(empty($username)){
            array_push($this->errors, ErrorsMessages::$emptyUsername);
        }elseif(!preg_match('/^[a-zA-Z\s]+$/', $username)){
            array_push($this->errors, ErrorsMessages::$usernamePaternChars);
        }elseif(strlen($username)<5 || strlen($username)>30){
            array_push($this->errors, ErrorsMessages::$usernamePaternLength);
        }elseif($this->checkIfUsernameIsTaken($username)){
            array_push($this->errors, ErrorMessages::$usernameAlreadyTaken);
        }else{
            return true;
        }
    }
    private function checkIfUsernameIsTaken($username){
        $sql = "SELECT id FROM users WHERE name = ?";
        if($stmt = $this->pdo->prepare($sql)){
            $stmt->bindParam(1, $username);
            if($stmt->execute()){
                if($stmt->rowCount()==1){
                    unset($stmt);
                    return true;
                }else{
                    unset($stmt);
                    return false;
                }
            }
        }
    }
    private function validateEmail($email){
        if(empty($email)){
            array_push($this->errors,ErrorMessages::$emailEmpty);  
        }
        elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            array_push($this->errors,ErrorMessages::$emailTaken);
        }elseif($this->checkIfEmailIsTaken($email)){
            array_push($this->errors,ErrorMessages::$emailTaken);
        }else{
            return true;
        }
    }
    private function checkIfEmailIsTaken($email){
        $sql = "SELECT id from users where email = ?";
        if($stmt = $this->pdo->prepare($sql)){
            $stmt->bindParam(1, $email);
            if($stmt->execute()){
                if($stmt->rowCount()==1){
                    unset($stmt);
                    return true;
                }else{
                    unset($stmt);
                    return false;
                }
            }
        }
    }

    private function validatePassword($password,$repeatPassword){
        if(empty($password)){
            array_push($this->errors,ErrorMessages::$passwordIsEmpty);
        }elseif($password!=$repeatPassword){
            array_push($this->errors, ErrorMessages::$diffrentPassAndRepeat);
        }elseif(!preg_match('/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{6,255}$/', $password)){
            array_push($this->errors, ErrorMessages::$passwordNotMeetPatern);
        }else{
            return true;
        }
    }
    private function insertToDatabase($username,$email,$password){
        $sql = "INSERT INTO users (name, email, password, role) VALUES (?,?,?,?)";
        if($stmt = $this->pdo->prepare($sql)){
            $username = trim($username);
            $password = password_hash($password, PASSWORD_DEFAULT);
            $userRole = 2;
            $stmt->bindParam(1,$username);
            $stmt->bindParam(2,$email);
            $stmt->bindParam(3,$password);
            $stmt->bindParam(4,$userRole);    
            if($stmt->execute()){
                unset($stmt);
                return true;
            }else{
                unset($stmt);
                return false;
            }
        }
    }
    public function getError($msg_error){
        if(!in_array($msg_error, $this->errors)){
            $msg_error = "";
        }
        return "<small class='form-text text-muted'>$msg_error</small>";
    }
}
?>