<?php 
class ErrorMessages{
    public static $emptyUsername = "Username is empty";
    public static $usernamePaternChars = "The user name can be only letters and spaces";
    public static $usernamePaternLength = "The user name must be at least 5 chars and max 30";
    public static $usernameAlreadyTaken = "The user name is already taken";
    public static $emailTaken = "Email address is already taken";
    public static $emailEmpty = "Email address is empty";
    public static $passwordIsEmpty = "Password is empty";
    public static $diffrentPassAndRepeat = "Password and repeat password is not this same";
    public static $passwordNotMeetPatern = "Password don't meet pattern";
}
?>