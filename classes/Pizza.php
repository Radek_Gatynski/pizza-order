<?php 
class Pizza{
    private $pdo;
    private $errors;
    public function __construct($conn){
        $this->pdo = $conn->connect();
        $this->errors = array();
    }
    public function addNewOrder($pizzaID, $customerID, $deliveryPlace, $sizePriceID, $totalPrice){
        $sql = "Insert Into ordered_pizzas (pizzaID, created_by, delivery_place, current_state, pizza_size, pizza_price) values (?,?,?,?,?,?) ";
        if($stmt = $this->pdo->prepare($sql)){
            $pizzaStatus = 'Ordered';
            $stmt->bindParam(1,$pizzaID);
            $stmt->bindParam(2,$customerID);
            $stmt->bindParam(3,$deliveryPlace);
            $stmt->bindParam(4, $pizzaStatus);
            $stmt->bindParam(5, $sizePriceID);
            $stmt->bindParam(6, $totalPrice);
            if($stmt->execute()){
                if($stmt->rowCount()==1){
                    return true;
                }
            }
        }else{
            return false;
        }
    }
    public function getOrders($userID){
        $getRole = $this->getUserRole($userID);
        if($getRole==2){
            $sql = "SELECT * FROM ordered_pizzas order by created_at DESC";
            if($stmt=$this->pdo->prepare($sql)){
                if($stmt->execute()){
                    $result = $stmt->fetchAll();
                    return $result;
                }
            }
        }elseif($getRole==1){
            $sql= "SELECT * FROM ordered_pizzas where created_by = ? order by created_at DESC";
            if($stmt=$this->pdo->prepare($sql)){
                $stmt->bindParam(1,$userID);
                if($stmt->execute()){
                    $result =  $stmt->fetchAll();
                    return $result;
                }
            }
        }else{
            return false;
        }
    }
    public function pizzasOrderDetails($pizzasID){
    $sql = "SELECT title, ingredients FROM  pizzas WHERE id LIKE $pizzasID";
       if($stmt = $this->pdo->prepare($sql)){
            if($stmt->execute()){
                return $stmt->fetchAll();
            }
        }
        return false;
    }
    private function getUserRole($id){
        $sql = "SELECT role from users where id = ?";
        if($stmt = $this->pdo->prepare($sql)){
            $stmt->bindParam(1, $id);
            if($stmt->execute()){
                if($stmt->rowCount()==1){
                   $stmt = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);
                   return $stmt[0];
                }
            }
        }
        return false;
    }
    public function getMenu(){
        $sql = "SELECT id, title, ingredients, price FROM pizzas";
        if($stmt = $this->pdo->prepare($sql)){
            if($stmt->execute()){
                $results = $stmt->fetchAll();
                return $results;
            }
        }
    }
    public function getPizzaSizes(){
        $sql = "SELECT * FROM pizza_size";
        if($stmt = $this->pdo->prepare($sql)){
            if($stmt->execute()){
                $results = $stmt->fetchAll();
                return $results;
            }
        }
    }
    public function changeStatus($id, $status){
        $sql = "UPDATE ordered_pizzas set current_state = ? where id = ?";
        if($stmt = $this->pdo->prepare($sql)){
            $stmt->bindParam(1, $status);
            $stmt->bindParam(2, $id);
            if($stmt->execute()){
                if($stmt->rowCount()==1){
                    return true;
                }
            }
        }
        return false;
    }
    public function delatePizza($id){
        $sql = "Delete from ordered_pizzas where id= ?";
        if($stmt = $this->pdo->prepare($sql)){
            $stmt->bindParam(1,$id);
            if($stmt->execute()){
                if($stmt->rowCount()==1){
                    return true;
                }
            }
        }
        return false;
    }
    public function getPizzaDetails($id){
        $result = $this->checkIfExist($id);
        if($result){
            $sql = "Select op.id, op.created_at, op.delivery_place, op.current_state, op.pizza_size, op.pizza_price, op.comment, p.title, p.ingredients, us.name, us.email from ordered_pizzas as op 
            INNER JOIN pizzas as p 
            ON op.pizzaID=p.id 
            INNER JOIN users us
            ON op.created_by=us.id    
            where op.id=$id";
            if($stmt = $this->pdo->prepare($sql)){
                $stmt->bindParam(1,$id);
                if($stmt->execute()){
                    return $stmt->fetchAll();
                }else{
                    return false;
                }
            }
        }else{
            return false;
        }
    }
    //checking if pizza exist and belonge to loged user
    private function checkIfExist($id){
        $sql = "Select id from ordered_pizzas where id = ? AND created_by = ?";
        if($stmt = $this->pdo->prepare($sql)){
            $stmt->bindParam(1,$id);
            $stmt->bindParam(2,$_SESSION['id']);
            if($stmt->execute()){
                if($stmt->rowCount()==1){
                    return true;
                }else{
                    return false;
                }
            }
        }
    }
}



?>