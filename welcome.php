<?php
session_start();
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}
?>
 
<!DOCTYPE html>
<html>
<body>
<?php include './templates/header.php'; ?>
    <div class="container-fluid text-center">
        <h1>Hi, <b><?php echo htmlspecialchars($_SESSION["username"]); ?></b></h1>
        <h3>Welcome to our site.</h3>
    </div>
<?php include './templates/footer.php'; ?>
</body>
</html>