<?php
include './classes/DBConfig.php';
$connect = new DBConfig();

include './classes/ErrorMessages.php';
include './classes/Login.php';
$event = new Login($connect);

include './templates/header.php';
include './handles/login-handler.php';
    
?>
<section class="container-fluid">
    <h4 class="text-center">Login</h4>
    <h5 class="text-center">Please fill fields to login</h5>
    <div id="login-form">
        <form action="login.php" method="POST"  class="form-container"> 
        <div class="form-grup">
            <label>Username: </label>
            <input  type="text" name="username"  class="form-control" require />
        </div>
        <div class="form-group">
            <label>Password: </label>
            <input  type="password" name="password"  class="form-control" require />
            <!--  value="<?php echo htmlspecialchars($password) ?>" -->
        </div>
            <div class="text-right">
                <input type="submit" name="login" value="submit" class="btn btn-primary">
            </div>
            <p>Don't have an account? <a href="register.php">Sign up here</a></p>
        </form>
    </div>
</section>

<?php include './templates/footer.php'; ?>
</body>
</html>