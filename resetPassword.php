<?php
session_start();
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}
$newPassword = $newPasswordConfirm = "";
$errors = array("newPasswordError" => "", "newPasswordConfirmError" => "");

if(isset($_POST['submit'])){
    // or if we want we can use $_SERVER["REQUEST_METHOD"] == "POST"
    if(empty($_POST['newPassword']))
        $errors['newPasswordError'] = "New password can't be empty";
    else{
        $newPassword = $_POST['newPassword'];
        if(!preg_match('/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{6,255}$/',$newPassword))
            $errors['newPasswordError'] = "Password does not meet condition";
    }
    if(empty($_POST['newPasswordConfirm']))
        $errors['newPasswordConfirmError'] = "Password can't be empty";
    else{
        $newPasswordConfirm = $_POST['newPasswordConfirm'];
        if(strcmp($newPasswordConfirm, $newPassword) != 0)
            $errors['newPasswordConfirmError'] = "Password and confirm password are not this same!";
    }
    if(!array_filter($errors)){
        include './DBconfig.php';
        //prevent put special char to DB
        $newPassword = mysqli_real_escape_string($conn, $_POST['newPassword']);
        $newPasswordConfirm = mysqli_real_escape_string($conn, $_POST['newPasswordConfirm']);
        $sql = "UPDATE users set password = ? where id = ?";
        if($stmt = mysqli_prepare($conn, $sql)){
            mysqli_stmt_bind_param($stmt, "si", $param_password, $param_id);
            $param_password = password_hash($newPassword, PASSWORD_DEFAULT);
            $param_id = $_SESSION["id"];
            if(mysqli_stmt_execute($stmt)){
                session_destroy();
                header("Location: login.php");
                exit();
            }else
                echo "Something went wrong";
            mysqli_stmt_close($stmt);
        }
      mysqli_close($link);
}
}

?>
<!DOCTYPE html>
<html>
<?php include './templates/header.php'; ?>
<section class="container-fluid">
    <h4 class="text-center">Reset password</h4>
    <div id="reset-password-form">
        <form action="resetPassword.php" method="POST">
            <div class="form-group">
                <label>New password: </label>
                    <input  type="password" class="form-control" name="newPassword" require value="<?php echo htmlspecialchars($newPassword) ?>">
                <div class='red-text'><?php echo $errors['newPasswordError'] ?></div>
            </div>
            <div class="form-group">
                <label>Repeat password: </label>
                    <input  type="password" class="form-control" name="newPasswordConfirm" require value="<?php echo htmlspecialchars($newPasswordConfirm) ?>">
                <div class='red-text'><?php echo $errors['newPasswordConfirmError'] ?></div>
            </div>
            <div class="text-center">
                <input type="submit" name="submit" value="submit" class="btn btn-danger">
            </div>
        </form>
    </div>
</section>
<?php include './templates/footer.php'; ?>
</html>