<?php
include './classes/DBConfig.php';
$connect = new DBConfig();

include './classes/ErrorMessages.php';
include './classes/Register.php';
$event = new Register($connect);

include './templates/header.php';
include './handles/register-handler.php';
?>
<section class="container-fluid">
    <h4 class="text-center">Sign up</h4>
    <div id="register-form">
        <form action="register.php" method="POST">
            <div class="form-group">
                <label>Your email: </label>
                <input  type="text" name="email" class="form-control" require require value="<?php echo $email;?>">
                <?php echo $event->getError(ErrorMessages::$emailEmpty);?>
                <?php echo $event->getError(ErrorMessages::$emailTaken);?>
            </div>
            <div class="form-group">
                <label>Username: </label>
                <input  type="text" name="username" class="form-control" require value="<?php echo $username;?>">
                <?php echo $event->getError(ErrorMessages::$usernameAlreadyTaken);?>
                <?php echo $event->getError(ErrorMessages::$usernamePaternChars);?>
                <?php echo $event->getError(ErrorMessages::$usernamePaternLength);?>
            </div>
            <div class="form-group">
                <label>Password: </label>
                <input  type="password" name="password" class="form-control" require require value="<?php echo $password;?>">
                <?php echo htmlspecialchars($event->getError(ErrorMessages::$passwordIsEmpty));?>
                <?php echo $event->getError(ErrorMessages::$diffrentPassAndRepeat);?>
                <?php echo $event->getError(ErrorMessages::$passwordNotMeetPatern);?>
            </div>
            <div class="form-group">
                <label>Repeat password: </label>
                <input  type="password" name="confirmPassword" class="form-control" require require value="<?php echo $confirmPassword;?>">
            </div>
            <div class="text-center">
                <input type="submit" name="register" value="submit" class="btn btn-primary">
                <input type="reset" name="reset" value="reset" class="btn btn-danger">
            </div>
            <p>Already have an account? <a href="login.php">Login here</a></p>
        </form>
        <a href='login.php' class="btn-login-register" style="display:none">Your account was created Click here to login<a>
    </div>
</section>
<?php include './templates/footer.php'; ?>
</html>