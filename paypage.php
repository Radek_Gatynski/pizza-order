<?php
session_start();
include './templates/header.php';
include './classes/DBConfig.php';
$conn = new DBConfig();
include './handles/payments-handler.php';
?>
<div class="container-fluid">
    <header>
        <h1 class="text-center">Payments</h1>
    </header>    
    <div class="container">
    <form action="paypage.php" method="post" id="payment-form"  style="width: 996px; margin: 0 auto" >
    
            <div class="form-group">
                <label for='orderID'>Order ID:</label>
                <input class="form-control StripeElement StripeElement--empty" type="number" name="orderID" id="orderID" placeholder="Order ID" required value="12" />
            </div>
         
            <div class="form-group">
                <label for="price">Price:</label>
                <input class="form-control  StripeElement StripeElement--empty" type="number" name="price" id="price" required placeholder="Price for order" value="12" />
            </div>
            <div id="card-element">
                <!-- A Stripe Element will be inserted here. -->
            </div>

            <!-- Used to display Element errors. -->
            <div id="card-errors" role="alert"></div>
      
        <input type="submit" name="submit" value="SUBMIT Payment" class="btn btn-primary" />
    </form>
    </div>
</div>
<?php include('./templates/footer.php'); ?>
</html>