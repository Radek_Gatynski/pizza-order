<!DOCTYPE html>
<html lang="en">
<?php
session_start();
include './templates/header.php';
include './classes/DBConfig.php';
$conn = new DBConfig();
include './classes/Pizza.php';
$pizza = new Pizza($conn);
include './handles/detail-pizza-handler.php';
include './handles/detail-pizza-change-status-handler.php';
include './handles/delete-pizza-handler.php';
?>

<div class="container-fluid">
    <div class="row">
        <div class="container pizza-details text-center"">
    <?php if ($pizzaDetail) : ?>
        <h2>Pizza details: </h2>
        <h4>Pizza name: <?php echo htmlspecialchars($pizzaDetail['title']); ?></h4>
        <p>Created by: <?php echo htmlspecialchars($pizzaDetail['email']); ?></p>
        <p>Created at: <?php echo date($pizzaDetail['created_at']); ?></p>
        <h5>Indgredients:</h5>
        <p><?php echo htmlspecialchars($pizzaDetail['ingredients']); ?></p>
        <p>Price <?php echo htmlspecialchars($pizzaDetail['pizza_price']); ?>zł</p>
        <p>Delivery place: <?php echo htmlspecialchars($pizzaDetail['delivery_place']); ?></p>

            <!--Comment pizza -->
            <?php if ($pizzaDetail['current_state'] == 'Canceled' || $pizzaDetail['current_state'] == 'Pizza delivered') : ?>
                <form action="" method=" POST">
                <div class="form-grup">
                    <label for="pizza-comment">Comment about pizza: </label>
                    <textarea id="pizza-comment" class="form-control" name="newComment" rows="4" cols="50" maxlength="500" placeholder="Please let us know how was your pizza (max: 500chars)"><?php echo htmlspecialchars_decode($pizzaDetail['comment']); ?></textarea>
                    <div class='text-danger'><?php if (!empty($commentInfo)) echo htmlspecialchars($commentInfo); ?></div>
                </div>
                <input type="hidden" name="commentToChange" value="<?php echo htmlspecialchars($pizzaDetail['id']); ?>">
                <input class="btn btn-info" type="submit" name="addComment" value="Change comment">
            </form>
            <?php endif; ?>

            <!-- change status in DB -->
            <?php if ($_SESSION['role'] == 1) : ?>
            <form action="details.php?id=<?php echo $_GET['id'];?>" method="POST">
                <label for="select-pizza-status">Order status</label>
                <select name="newStatus" id="select-pizza-status" class="form-control">
                    <option value="" disabled selected>Current state:
                        <?php echo htmlspecialchars($pizzaDetail['current_state']); ?></option>
                    <option value="Ordered">Ordered</option>
                    <option value="In progress">In progress</option>
                    <option value="Pizza went to the client">Pizza went to the client</option>
                    <option value="Pizza delivered">Pizza delivered</option>
                    <option value="Canceled">Canceled</option>
                </select>
                <input type="hidden" name="pizzaToChange" value="<?php echo htmlspecialchars($pizzaDetail['id']); ?>">
                <input type="submit" name="changeStatus" value="Change status" class="btn btn-primary">
            </form>
            <!--delete from DB -->
            <form action="details.php" method="POST">
                <input type="hidden" name="id_to_delete" value="<?php echo htmlspecialchars($pizzaDetail['id']); ?>">
                <input type="submit" name="delete" value="Delete pizza" class="btn btn-danger"><br />
                <div class='text-danger'><?php if (!empty($deleteComment)) echo htmlspecialchars($deleteComment); ?>
                </div>
            </form>
            <?php endif; ?>
            <?php else : ?>
            <h4>Pizza not exist</h4>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php include('templates/footer.php'); ?>

</html>