<!DOCTYPE html>
<html lang="en">
<?php 
include './templates/header.php';
include './classes/DBConfig.php';
$conn = new DBConfig();
include './classes/Pizza.php';
$menu = new Pizza($conn);
$pizzasMenu = $menu->getMenu();
$pizzasSize = $menu->getPizzaSizes();
?>
<section class="container-fluid">
    <h4 class="text-center">Our pizzas</h4>
    <table class="table" style="max-width: 996px; margin: 0 auto"   >
        <thead class="thead-light">
          <tr>              
              <th>Title</th>
              <th>Ingredients</th>
              <th>Price</th>
          </tr>
        </thead>

        <tbody>
        <?php foreach($pizzasMenu as $pizza): ?>
          <tr id="pizzaListTable">
            <td><?php echo htmlspecialchars($pizza['title']); ?></td>
            <td><?php echo htmlspecialchars($pizza['ingredients']); ?></td>
            <td class='price'><?php echo htmlspecialchars($pizza['price']) . 'zł'; ?></td>
          </tr>
        <?php endforeach; ?>
        <tr id="selectedSize">
            <?php foreach($pizzasSize as $size): ?>
                    <td>
                      <span><?php echo $size['name'] . " + " . $size['price_plus'] . "zl" ; ?> </span>
                    </td>
            <?php endforeach; ?>
        </tr>
        </tbody>
      </table>
</section>
<?php
include('./templates/footer.php');
?>
</html>