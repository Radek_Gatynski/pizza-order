<?php
    session_start();
    if(!isset($_SESSION['username']) || ($_SESSION['id'] != $_GET['id']))
          header("location: index.php");

    include('DBconfig.php');
    if(isset($_GET['id'])){ 
        $id = mysqli_real_escape_string($conn, $_GET['id']);
       $sql = "Select name, email, created_at, image_name, profile_picture from users where id = '$id'";
       $results = mysqli_query($conn, $sql);
       $userInfo = mysqli_fetch_assoc($results);
       mysqli_free_result($results);
     }
    
    if(isset($_POST["submit"])){
        $id = mysqli_real_escape_string($conn, $_GET['id']);
        $check = getimagesize($_FILES["image"]["tmp_name"]);
        if($check !== false){
            $image = $_FILES['image']['tmp_name'];
            $imgName = addslashes($_FILES['image']['name']);
            $imgContent = addslashes(file_get_contents($image));
            $sql = "Update users SET profile_picture = '$imgContent', image_name = '$imgName' where id = $id";
            if(mysqli_query($conn, $sql)){
                header("Refresh:0");
            }else{
                echo "File upload failed, please try again.";
            } 
        }else{
            echo "Please select an image file to upload.";
        }
    }
    mysqli_close($conn);  
?>

<!DOCTYPE html>
<html>
<?php include './templates/header.php'; ?>

<div class="container-fluid">
<div class="row">
        <div class="userPersonalInfo container">
                <div class="row">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <?php if($userInfo): ?>
                            <?php echo '<img height="195" width="195" src="data:image/jpeg;base64,'.base64_encode( $userInfo['profile_picture'] ).'"/>'; ?>
                        <?php else: ?>
                            <img src="./assets/pizza_man.jpg" height="150" width="100" alt="profile picture" />
                        <?php endif; ?>
                        <form class="pick-new-image" action="" method="POST" enctype="multipart/form-data">    
                            <div class="form-group">
                                <label for="file"><strong>Choose a file</strong></label>
                                <input type="file" name="image" id="file"class="btn btn-info btn-sm"><br />
                            </div>
                            <label for="submit"></label>
                            <input id="submit" class="btn btn-primary btn-sm" type="submit" name="submit" value="Upload">
                        </form>
                    </div>
                    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-6 col-xs-12">
                        <h5>Name: <?php echo htmlspecialchars($userInfo['name']); ?> </h5>
                        <h5>Email: <?php echo  htmlspecialchars($userInfo['email']); ?> </h5>
                        <h5>Created at: <?php echo  htmlspecialchars($userInfo['created_at']); ?> </h5>
                        <a class='btn btn-info' href='resetPassword.php'>Reset Password</a>
                        <a class='btn btn-info' href='index.php'>Your ordered pizzas</a>
                    </div>     
                </div>
        </div>
</div>
</div>

<?php include './templates/footer.php'; ?>
</html>