<?php 
session_start();
include './templates/header.php';
include './classes/DBConfig.php';
$conn = new DBConfig();
include './classes/Pizza.php';
$pizza = new Pizza($conn);
include './handles/orders-pizza-handler.php';
$userID = $_SESSION['id'];
?>
<div class="container-fluid">
    <?php if(!empty($userID)): ?>
    <h4 class="text-center">Your Pizzas!</h4>
    <div class="row">
        <?php $index = 0; ?>
        <div class="container">
        <div class="row orders-page-pizzas-info">
        <?php foreach($ordered_pizzas as $order): ?>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="card">
                    <?php if($order['current_state']=='Pizza delivered' || $order['current_state']=='Canceled'): ?>
                    <div class="card-body text-center pizza-delivered"> 
                    <?php else: ?>
                    <div class="card-body text-center pizza-still-in-progress">
                    <?php endif; ?>
                        <h5><?php echo htmlspecialchars($tmpPizza[$index]['title']); ?> <small>Order number: <?php echo htmlspecialchars($order['id']); ?></small></h5>
                        <p>Ordered at: <?php echo htmlspecialchars($order['created_at']); ?></p>
                        <p>Status: <?php echo htmlspecialchars($order['current_state']); ?></p> 
                        <small>Delivery place <?php echo htmlspecialchars($order['delivery_place']); ?></small>
                        <ul class="list-unstyled ">
                            <?php foreach(explode(',', $tmpPizza[$index]['ingredients']) as $ing) : ?>
                            <li><?php echo htmlspecialchars($ing); ?></li>
                            <?php endforeach ?>  
                        </ul>
                        <h6>Price: <?php echo htmlspecialchars($order['pizza_price']); ?></h6>
                    </div>
                    <a href="details.php?id=<?php echo htmlspecialchars($order['id']); ?>" class='text-right text-uppercase'>more info</a>            
                </div>
            </div>
            <?php $index++; ?>
         
         <?php endforeach; ?>
        </div>
        </div>
    </div> 

    <?php else: ?>
    <div class="text-center">
        <h3>Welcome on our website</h3>
        <p>To order some pizza please <a href="login.php">login</a> to our website or if you don't have account please <a href="register.php">register</a></p>
        <img src='./assets/pizza 2.png' alt="pizza icon" />
    </div>
    <?php endif; ?>
</div>

<?php include('./templates/footer.php'); ?>
</html>
